import { chunk, get, isNumber, property as prop } from 'lodash'
import superagent from 'superagent'

const MAX_CHUNK_SIZE = 20

export function consolidatePayload(payload) {
  if (Array.isArray(payload)) {
    const payloadIds = payload.map(prop('id'))
    const maxId = Math.max(
      ...payloadIds.filter((command) => isNumber(command)),
      0
    )

    return payload.map((command, index) => {
      const id = isNaN(command.id) ? maxId + index + 1 : command.id

      return { ...command, id, jsonrpc: '2.0' }
    })
  }

  return { ...payload, id: 1, jsonrpc: '2.0' }
}

function isErrorResult(result) {
  return typeof result.error === 'object'
}

function isBatchResult(result) {
  return Array.isArray(result)
}

function isBatchPayload(payload) {
  return Array.isArray(payload)
}

function concatBatchResults(aggregatedResults, result) {
  const commandResult = get(result, 'result', [])

  return aggregatedResults.concat(commandResult)
}

class RpcApiClient {
  constructor(config) {
    this.config = {
      baseUrl: 'api-boutique-dev.kumbu.fr/rpc',
    }

    this.listeners = {}

    this.config = {
      debug: false,
      ...config,
    }
  }

  /**
   * Generates the JSON RPC Api Enpoint Url
   * @param payload
   * @returns {string}
   */
  getRPCEndpointUrl(payload) {
    let endpointUrl = this.config.baseUrl

    const query = {}

    if (this.config.debug) {
      // This is only extra logging information to quickly identify rpc queries
      if (Array.isArray(payload)) {
        query.methods = payload.map(prop('method')).join(',')
      } else if (payload.method) {
        query.method = payload.method
      }
    }

    const finalQueryKeys = Object.keys(query).filter((key) => query[key])

    if (finalQueryKeys.length > 0) {
      const requestQuery = finalQueryKeys
        .map((key) => `${key}=${query[key]}`)
        .join('&')

      endpointUrl = `${endpointUrl}?${requestQuery}`
    }

    return endpointUrl
  }

  /**
   * Builds and sends the JSON-RPC command
   * @param {Object} payload
   * @param {boolean} ignoreErrors
   * @returns {Promise} a p
   */
  sendRequest(payload) {
    const request = superagent
      .post(this.getRPCEndpointUrl(payload))
      .set('Accept', 'application/json')

    if (this.config.authToken) {
      request.set('Authorization', `Bearer ${this.config.authToken}`)
    }

    return new Promise((resolve, reject) =>
      request.send(consolidatePayload(payload)).end((err, res) => {
        const onError = (err) => {
          reject(err)
          this.notify('error', err)
        }

        if (err || res.body.error) {
          if (!res) {
            return onError(err)
          }

          const jsonRpcResponseError = get(res, 'body.error', {})
          const defaultErrMessage = get(err, 'message')

          return onError({
            httpStatus: res.status,
            originalPayload: payload,
            message: get(jsonRpcResponseError, 'message', defaultErrMessage),
            ...jsonRpcResponseError,
          })
        }

        return resolve(res.body)
      })
    )
  }

  /**
   * Sends a JSON-RPC command to JSON-RPC api
   * @param method
   * @param params
   * @param id
   * @returns {Promise.Object}
   */
  sendCommand(command) {
    return this.sendRequest(command).then(prop('result'))
  }

  /**
   * Send several JSON-RPC in one batch
   * @param commands
   * @param ignoreErrors
   * @returns {Promise}
   */
  async batchCommands(commands, ignoreErrors = true) {
    if (!Array.isArray(commands) || commands.length === 0) {
      throw new Error('Batch payload can not be empty')
    }

    let results = []

    for await (const chunkResults of this.sendChunk(commands)) {
      results = results.concat(chunkResults)
    }

    const errorResult = results.find(isErrorResult)

    if (errorResult && !ignoreErrors) {
      throw new Error(errorResult.error.message)
    }

    return results
  }

  async *sendChunk(payload, mapChunk, chunkSize = MAX_CHUNK_SIZE) {
    const chunks = chunk(payload, chunkSize)

    for (const payload of chunks) {
      const chunk = isBatchPayload(payload) ? payload : mapChunk(payload)

      yield await this.sendRequest(chunk)
    }
  }

  /**
   * Will map over chunks of items to trigger a request for each chunk
   * @param {*} items
   * @param {*} mapChunk
   * @param {*} chunkSize
   */
  async sendBulk(items, mapChunk, notifyProgress, chunkSize = MAX_CHUNK_SIZE) {
    let results = []

    const chunksCount = Math.ceil(items.length / chunkSize)

    let chunkNo = 0

    for await (const response of this.sendChunk(items, mapChunk, chunkSize)) {
      if (typeof notifyProgress === 'function') {
        notifyProgress((Number(chunkNo) / chunksCount) * 100)
      }

      if (isBatchResult(response)) {
        results = results.concat(response.reduce(concatBatchResults, []))
      } else if (!isErrorResult(response)) {
        results = results.concat(response.result)
      }

      chunkNo++
    }

    return results
  }

  notify(msg, eventData) {
    const listeners = this.listeners[msg] || []

    listeners.map((listenerFunc) => listenerFunc(eventData))
  }

  on(msg, listenerFn) {
    if (typeof msg === 'string' && msg && typeof listenerFn === 'function') {
      this.listeners[msg] = (this.listeners[msg] || []).concat(listenerFn)

      return () => {
        this.listeners[msg] = this.listeners[msg].filter(
          (fn) => fn !== listenerFn
        )
      }
    }
    throw new Error(
      'on should be supplied a valid message (string) and a listener (function)'
    )
  }

  setConfigKey(key, val) {
    this.config[key] = val
  }

  getConfigKey(key) {
    return this.config[key]
  }

  unsetConfigKey(key) {
    delete this.config[key]
  }
}

export default RpcApiClient

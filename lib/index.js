import 'babel-polyfill'
import RpcClient from './client'

const client = new RpcClient({
  baseUrl: 'https://api-boutique-dev.kumbu.fr/rpc',
})

function sendRpcCommand(method, params) {
  return client.sendCommand({
    method,
    params,
    id: 1,
    jsonrpc: '2.0',
  })
}

function getClient() {
  return {
    requestApplicationUpload(payload) {
      return sendRpcCommand('requestApplicationUpload', payload)
    },
    fetchPosts(payload) {
      return sendRpcCommand('listPosts', {
        lang: 'fr',
        domain: 'excelya',
        ...payload,
      })
    },
  }
}

async function bindContentBlock(opts) {
  if (typeof opts !== 'object' || typeof opts.container !== 'string') {
    throw new Error('Options must be an object')
  }

  const { name, container: containerSelector, count = 5 } = opts

  let container

  try {
    container = document.querySelector(containerSelector)
  } catch (err) {
    console.log(
      `You need do provide a valid selector to bind content block ${name}`
    )
  }

  const contents = await getClient().fetchPosts()

  container.innerHTML = `
    <ul class="PostsList">
      ${contents
        .slice(0, count)
        .map(
          (content) =>
            `<li class="PostsList__Item"><a href="${
              content.url || '#'
            }" class="PostsList__Item__Link">${content.title}</a></li>`
        )
        .join('\n')}
    </ul>
  `

  return container
}

window.Kumbu = {
  getClient,
  bindContentBlock,
}
